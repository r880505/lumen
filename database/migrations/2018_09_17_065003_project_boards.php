<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectBoards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_boards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid',50)->unique();
            $table->unsignedInteger('project');
            $table->foreign('project')->references('id')->on('projects');
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_boards');
        //
    }
}
